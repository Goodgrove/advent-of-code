﻿module Day6.Implementation

(*
Because your neighbors keep defeating you in the holiday house decorating contest year after year, you've decided to deploy one million lights in a 1000x1000 grid.

Furthermore, because you've been especially nice this year, Santa has mailed you instructions on how to display the ideal lighting configuration.

Lights in your grid are numbered from 0 to 999 in each direction; the lights at each corner are at 0,0, 0,999, 999,999, and 999,0. The instructions include whether to turn on, turn off, or toggle various inclusive ranges given as coordinate pairs. Each coordinate pair represents opposite corners of a rectangle, inclusive; a coordinate pair like 0,0 through 2,2 therefore refers to 9 lights in a 3x3 square. The lights all start turned off.

To defeat your neighbors this year, all you have to do is set up your lights by doing the instructions Santa sent you in order.

For example:

    turn on 0,0 through 999,999 would turn on (or leave on) every light.
    toggle 0,0 through 999,0 would toggle the first line of 1000 lights, turning off the ones that were on, and turning on the ones that were off.
    turn off 499,499 through 500,500 would turn off (or leave off) the middle four lights.

After following the instructions, how many lights are lit?
*)


type point = { x : int; y : int; }

type rectangle = { x1:int; y1:int; x2:int; y2:int; }

let initGrid (w:int) (h:int) = List.init h (fun _ -> List.init w (fun _ -> false))

let turnOn bulb = true
let turnOff bulb = false
let toggle bulb = not bulb
let id bulb = bulb

type CommandType =
    | TurnOn
    | TurnOff
    | Toggle
    | Nothing

let parseCommand (str : string) =
    let subStrMatch (str2 : string) =
        str.Substring(0, str2.Length) = str2
    match str with
        | _ when subStrMatch "turn on" -> TurnOn
        | _ when subStrMatch "turn off" -> TurnOff
        | _ when subStrMatch "toggle" -> Toggle
        | _ -> Nothing

let getTransformer command =
    match command with
        | TurnOn -> turnOn
        | TurnOff -> turnOff
        | Toggle -> toggle
        | Nothing -> id

let parseBounds (str : string) =
    let matches = System.Text.RegularExpressions.Regex.Matches (str, "(\d+),(\d+) through (\d+),(\d+)")
    let x1 = int matches.[0].Groups.[1].Value
    let y1 = int matches.[0].Groups.[2].Value
    let x2 = int matches.[0].Groups.[3].Value
    let y2 = int matches.[0].Groups.[4].Value
    { x1 = x1; y1 = y1; x2 = x2; y2 = y2; }

let inBounds (rect : rectangle) (point : point) =
    point.x >= rect.x1 && point.x <= rect.x2 &&
    point.y >= rect.y1 && point.y <= rect.y2

let transformSubGrid (transformer : 'a -> 'a) (grid : list<list<'a>>) (subGrid:rectangle) =
    grid |>
    List.mapi (fun rowIdx row ->
        row |> List.mapi (fun colIdx cell ->
            let point = { x=colIdx; y=rowIdx; }
            if (inBounds subGrid point) then
                transformer cell
            else
                id cell))