﻿module Day6.ImplementationPart2

(*
You just finish implementing your winning light pattern when you realize you mistranslated Santa's message from Ancient Nordic Elvish.

The light grid you bought actually has individual brightness controls; each light can have a brightness of zero or more. The lights all start at zero.

The phrase turn on actually means that you should increase the brightness of those lights by 1.

The phrase turn off actually means that you should decrease the brightness of those lights by 1, to a minimum of zero.

The phrase toggle actually means that you should increase the brightness of those lights by 2.

What is the total brightness of all lights combined after following Santa's instructions?

For example:

    turn on 0,0 through 0,0 would increase the total brightness by 1.
    toggle 0,0 through 999,999 would increase the total brightness by 2000000.

*)

open Day6.Implementation

let turnUp n = n + 1
let turnDown n =
    match n with
        | 0 -> 0
        | v -> v - 1
let turnUp2 n = n + 2


let getPart2Transformer command =
    match command with
        | TurnOn -> turnUp
        | TurnOff -> turnDown
        | Toggle -> turnUp2
        | Nothing -> id

let initGridPart2 w h =
    List.init h (fun _ ->
        List.init w (fun _ -> 0))
