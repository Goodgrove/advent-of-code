﻿namespace Day6.Tests

open Day6.Implementation
open NUnit.Framework
open Swensen.Unquote

[<TestFixture>]
type public BoundsTests () =

    [<TestCase(2,2)>]
    [<TestCase(3,5)>]
    [<TestCase(5,5)>]
    member public this.IsInBounds (x:int, y:int) =
        let rect1 = { x1 = 2; y1 = 2; x2 = 5; y2 = 5; }
        let point1 = { x = x; y = y }
        inBounds rect1 point1 =! true

    [<TestCase(20,20)>]
    [<TestCase(6,5)>]
    [<TestCase(5,6)>]
    [<TestCase(1,3)>]
    member public this.NotInBounds (x:int, y:int) =
        let rect1 = { x1 = 2; y1 = 2; x2 = 5; y2 = 5; }
        let point1 = { x = x; y = y }
        inBounds rect1 point1 =! false

    [<TestCase(2,2)>]
    member public this.NotInBounds2 (x:int, y:int) =
        let rect1 = { x1 = 3; y1 = 2; x2 = 5; y2 = 5; }
        let point1 = { x = x; y = y }
        inBounds rect1 point1 =! false