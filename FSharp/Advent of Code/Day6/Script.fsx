﻿// Learn more about F# at http://fsharp.net. See the 'F# Tutorial' project
// for more guidance on F# programming.
#load "Implementation.fs"
#load "ImplementationPart2.fs"

open Day6.Implementation
open Day6.ImplementationPart2

let fileLines = System.IO.File.ReadAllLines "PuzzleInput.txt" |> Array.filter (fun x -> x <> "")

let sw1 = new System.Diagnostics.Stopwatch ()
sw1.Start ()
let grid = initGrid 1000 1000

// To do this we will need to fold over the fileLines using the grid as the accumulator

let transforms =
    fileLines |>
        Array.map (fun line ->
            ((getTransformer <| parseCommand line), parseBounds line))

let result =
    transforms |>
        Array.fold (fun grid2 (transform, region) ->
            transformSubGrid transform grid2 region) grid

let count = result |> List.map (fun x -> x |> List.filter (fun x -> x) |> List.length) |> List.sum
sw1.Stop ()

printfn "Part1: %d" count
printfn "%f (s) elapsed" sw1.Elapsed.TotalSeconds

let sw2 = new System.Diagnostics.Stopwatch ()
sw2.Start ()

let luminanceGrid = initGridPart2 1000 1000

let transformsPart2 =
    fileLines |>
    Array.map (fun line ->
        ((getPart2Transformer <| parseCommand line), parseBounds line))

let result2 =
    transformsPart2 |>
    Array.fold (fun grid2 (transform, region) ->
        transformSubGrid transform grid2 region) luminanceGrid

let sum = result2 |> List.map (fun x -> List.sum x) |> List.sum
sw2.Stop ()

printfn "Part2: %d" sum
printfn "%f (s) elapsed" sw2.Elapsed.TotalSeconds