﻿namespace Day6.Tests

open Day6.Implementation
open NUnit.Framework
open Swensen.Unquote

[<TestFixture>]
type public ParsingTests () =

    [<Test>]
    member public this.ParseTurnOnCommand () =
        let command = parseCommand "turn on 0,0 through 999,999"
        command =! TurnOn

    [<Test>]
    member public this.ParseTurnOffCommand () =
        let command = parseCommand "turn off 499,499 through 500,500"
        command =! TurnOff

    [<Test>]
    member public this.ParseToggleCommand () =
        let command = parseCommand "toggle 0,0 through 999,0"
        command =! Toggle

    [<Test>]
    member public this.ParseNoCommand () =
        let command = parseCommand "nothing to parse"
        command =! Nothing

    [<Test>]
    member public this.ParseIt () =
        let x = parseBounds "turn off 499,499 through 500,500"
        x =! {x1=499;y1=499;x2=500;y2=500}