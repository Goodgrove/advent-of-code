﻿module Day3.Implementation

type Direction =
    | North
    | South
    | East
    | West

let charToDirection char =
    match char with
        | '<' -> Some(West)
        | '>' -> Some(East)
        | 'v' -> Some(South)
        | '^' -> Some(North)
        | _ -> None

let transformCoords (x:int, y:int) (dir:Direction) =
    match dir with
        | North -> (x, y + 1)
        | South -> (x, y - 1)
        | East -> (x + 1, y)
        | West -> (x - 1, y)

let coordsVisitedFromJourneyString (inputStr : string) =
    let journey1 = ([(0, 0)])
    inputStr |>
    Seq.map charToDirection |>
    Seq.choose id |>
    Seq.fold 
        (fun newList direction ->
            let previousCoords = Seq.head newList
            let newCoords = transformCoords previousCoords direction
            (newCoords :: newList))
        journey1 |>
    Seq.distinct