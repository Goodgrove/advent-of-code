﻿namespace Day3.UnitTests
open Day3.Implementation
open FsUnit
open NUnit.Framework

[<TestFixture>]
type public DirectionParsing() = 

    [<Test>]
    member public this.ParseNorth() =
        charToDirection '^' |> should equal (Some North)

    [<Test>]
    member public this.ParseSouth() =
        charToDirection 'v' |> should equal (Some South)

    [<Test>]
    member public this.ParseEast() =
        charToDirection '>' |> should equal (Some East)

    [<Test>]
    member public this.ParseWest() =
        charToDirection '<' |> should equal (Some West)

[<TestFixture>]
type public CoordinateTransformation() =
    [<Test>]
    member public this.MoveNorth() =
        transformCoords (0, 0) North |> should equal (0, 1)
    [<Test>]
    member public this.MoveSouth() =
        transformCoords (0, 0) South |> should equal (0, -1)
    [<Test>]
    member public this.MoveEast() =
        transformCoords (0, 0) East |> should equal (1, 0)
    [<Test>]
    member public this.MoveWest() =
        transformCoords (0, 0) West |> should equal (-1, 0)