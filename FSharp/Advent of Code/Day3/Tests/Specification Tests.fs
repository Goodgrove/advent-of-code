﻿namespace Day3
open Day3.Implementation
open FsUnit
open NUnit.Framework

[<TestFixture>]
type Specification_Tests() =

    [<Test>]
    member public this.Test1 () =
        let result = coordsVisitedFromJourneyString ">" |> Seq.length
        result |> should equal 2

    [<Test>]
    member public this.Test2 () =
        let result = coordsVisitedFromJourneyString "^>v<" |> Seq.length
        result |> should equal 4

    [<Test>]
    member public this.Test3 () =
        let result = coordsVisitedFromJourneyString "^v^v^v^v^v" |> Seq.length
        result |> should equal 2
