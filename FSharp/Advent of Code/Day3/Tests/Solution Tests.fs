﻿namespace Day3.SolutionTests
open System.Reflection
open System.IO
open Day3.Implementation
open FsUnit
open NUnit.Framework

[<TestFixture>]
type public Solution() = 
    let sourceText =
        let ass = Assembly.GetExecutingAssembly()
        let stream = ass.GetManifestResourceStream("Route.txt")
        use reader = new StreamReader(stream)
        reader.ReadToEnd()

    [<Test>]
    member public this.Solution() =
        let result = coordsVisitedFromJourneyString sourceText |> Seq.length
        result |> should equal 2572

    [<Test>]
    member public this.Solution2() =
        let (santaJourney, roboSantaJourney) =
            sourceText |>
                Seq.mapi (fun idx char -> (idx, char)) |>
                Seq.toList |>
                List.partition (fun (idx, char) -> idx % 2 = 0)
        let j1 = new string (santaJourney |> Seq.map (fun (idx, char) -> char) |> Seq.toArray)
        let j2 = new string (roboSantaJourney |> Seq.map (fun (idx, char) -> char) |> Seq.toArray)
        let coords1 = coordsVisitedFromJourneyString j1
        let coords2 = coordsVisitedFromJourneyString j2
        let result = Seq.append coords1 coords2 |> Seq.distinct |> Seq.length

        result |> should equal 2631
