﻿namespace Day2SpecTests

open Day2.PaperCalculator
open FsUnit
open NUnit.Framework

[<TestFixture>]
type Day2SpecificationTests() = 

    [<Test>]
    member public this.Part1Test1() =
        let box = new Box(2, 3, 4)
        let result = BoxPaperCalculator.TotalPaperRequired(box)
        result |> should equal 58

    [<Test>]
    member public this.Part1Test2() =
        let box = new Box(1, 1, 10)
        let result = BoxPaperCalculator.TotalPaperRequired(box)
        result |> should equal 43

    [<Test>]
    member public this.Part2Test1() =
        let box = new Box(2, 3, 4)
        let result = BoxRibbonCalculator.TotalRibbonRequired (box)
        result |> should equal 34

    [<Test>]
    member public this.Part2Test2() =
        let box = new Box(1, 1, 10)
        let result = BoxRibbonCalculator.TotalRibbonRequired(box)
        result |> should equal 14