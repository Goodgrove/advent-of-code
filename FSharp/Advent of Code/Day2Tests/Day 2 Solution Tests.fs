﻿
namespace Day2Tests

open System
open System.IO
open System.Reflection
open Day2.PaperCalculator
open FsUnit
open NUnit.Framework

[<TestFixture>]
type Day2SolutionTests() =

    let sourceText =
        let ass = Assembly.GetExecutingAssembly()
        let stream = ass.GetManifestResourceStream("TestSource.txt")
        use reader = new StreamReader(stream)
        reader.ReadToEnd()

    let lines =
        sourceText.Split('\n') |>
            Seq.filter (fun s -> String.IsNullOrWhiteSpace(s) = false)

    [<Test>]
    member public this.Part1Solution () =
        let result =
            lines |>
                Seq.map (fun s -> Box.Parse(s)) |>
                Seq.map (fun box -> BoxPaperCalculator.TotalPaperRequired(box)) |>
                Seq.sum
        result |> should equal 1598415

    [<Test>]
    member public this.Part2Solution () =
        let result =
            lines |>
                Seq.map (fun s -> Box.Parse(s)) |>
                Seq.map (fun box -> BoxRibbonCalculator.TotalRibbonRequired(box)) |>
                Seq.sum
        result |> should equal 3812909