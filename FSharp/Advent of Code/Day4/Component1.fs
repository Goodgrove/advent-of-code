﻿module Day4.Implementation

open System.Security.Cryptography

let findNextCoinHashInteger (secretKey : string) n =
    let hashStartsNZeros n (hash : byte[]) =
        let n1 = n / 2
        hash |>
            Array.take n1 |>
            Array.forall (fun b -> b = (byte)0) &&
            (
            n % 2 = 0 ||
            (hash |>
                Array.skip n1 |>
                Array.head) < (byte)16)
    
    use md5sum = MD5.Create()
    let md5 secret index =
        let str = secret + (index.ToString())
        let bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(str)
        md5sum.ComputeHash(bytes)

    let (index, hash) =
        seq { 1 .. System.Int32.MaxValue } |>
        Seq.map (fun i -> (i, md5 secretKey i)) |>
        Seq.filter (fun (i, h) -> hashStartsNZeros n h) |>
        Seq.head
    index
