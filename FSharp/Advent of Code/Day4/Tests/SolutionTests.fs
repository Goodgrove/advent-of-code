﻿namespace Day4
open System
open Day4.Implementation
open NUnit.Framework
open Swensen.Unquote

[<TestFixture>]
type SolutionTests() = 
    [<Test>]
    member public this.Part1() =
        let result = findNextCoinHashInteger "ckczppom" 5
        result =! 117946

    [<Test>]
    member public this.Part2() =
        let result = findNextCoinHashInteger "ckczppom" 6
        result =! 3938038