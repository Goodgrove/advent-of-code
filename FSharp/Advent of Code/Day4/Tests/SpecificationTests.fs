﻿

namespace Day4
open System
open Day4.Implementation
open NUnit.Framework
open Swensen.Unquote

[<TestFixture>]
type public SpecificationTests() = 

    [<Test>]
    member public this.Test1() =
        let result = findNextCoinHashInteger "abcdef" 5
        result =! 609043

    [<Test>]
    member public this.Test2() =
        let result = findNextCoinHashInteger "pqrstuv" 5
        result =! 1048970