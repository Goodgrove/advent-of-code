﻿namespace Day_1

type public FloorCalculator() = 
    member public this.CalculateFloor (commandSeq : string) =
        commandSeq.ToCharArray() |>
            Seq.map (fun c -> 
                match c with
                    | '(' -> 1
                    | ')' -> -1
                    | _ -> 0
            ) |>
            Seq.sum
    
    member public this.NextFloor (startFloor : int, command : char) =
        match command with
            | '(' -> startFloor + 1
            | ')' -> startFloor - 1
            | _ -> startFloor

    member public this.FirstHitBasement(startFloor, commandSeq : string) =
        let mapped =
            commandSeq.ToCharArray() |>
                Seq.mapi (fun index commandChar ->
                    let subString = commandSeq.Substring (0, index + 1)
                    let floor = this.CalculateFloor(subString)
                    (index, floor)
                )
        let result = Seq.tryFind (fun (index, floor) -> floor = -1) mapped
        match result with
            | None -> -1
            | Some (index, floor) -> index + 1