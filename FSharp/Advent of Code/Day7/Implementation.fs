﻿module Day7.Implementation

type Operation =
    | AND
    | LSHIFT
    | NOT
    | OR
    | RSHIFT

type Signal =
    | Signal of int

type Wire =
    | Wire of string * Signal

type Inputs =
    | SignalInput of Signal
    | WireInput of Wire


type Token =
    | AssignmentToken
    | OperatorToken of Operation
    | SignalToken of int
    | WireToken of string
    | UnrecognisedToken

let parseWire parseStr =
    parseStr

let parseInputs parseStr =
    parseStr

let parseOperator parseStr =
    match parseStr with
        | "AND" -> Some(AND)
        | "LSHIFT" -> Some(LSHIFT)
        | "NOT" -> Some(NOT)
        | "OR" -> Some(OR)
        | "RSHIFT" -> Some(RSHIFT)
        | _ -> None

let parseSignal parseStr =
    int parseStr

let parseToken tokenStr =
    let isDigit c = System.Char.IsDigit (c)
    let isLower c = System.Char.IsLower (c)
    let isUpper c = System.Char.IsUpper (c)

    let (|IsAssignment|_|) s =
        if (s = "->") then
            Some (true)
        else
            None
    let (|IsSignal|_|) s =
        if (s |> Seq.forall isDigit) then
            Some (parseSignal s)
        else
            None
    let (|IsWire|_|) s =
        if (s |> Seq.forall isLower) then
            Some (parseWire s)
        else
            None
    let (|IsOperation|_|) s =
        if (s |> Seq.forall isUpper) then
            parseOperator s
        else
            None

    match tokenStr with
        | IsAssignment _ -> AssignmentToken
        | IsSignal signal-> SignalToken signal
        | IsWire w -> WireToken w
        | IsOperation o -> OperatorToken o
        | _ -> UnrecognisedToken


let parseString (inputStr : string) =
    let tokens = inputStr.Split(' ') |> Seq.filter (fun s -> not (System.String.IsNullOrWhiteSpace (s)) ) |> Seq.map parseToken
    let statement =
        tokens |>
        Seq.takeWhile (fun t -> match t with | AssignmentToken -> false | _ -> true)
    let inputs =
        statement |>
        Seq.where (fun t -> match t with | SignalToken s -> true | WireToken w -> true | _ -> false)
    let operation =
        statement |>
            Seq.tryFind (fun t -> match t with | OperatorToken o -> true | _ -> false)
    let output =
        tokens |>
        Seq.skipWhile (fun t -> match t with | AssignmentToken -> false | _ -> true) |>
        Seq.skip 1 |>
        Seq.head
    (inputs, operation, output)

let parseTokens inputTokens operationToken outputToken =
    let OperationToken operation = operationToken

    let parseI i1 =
        match i1 with
        | Some (SignalToken sigVal) -> Some (SignalInput (Signal sigVal))
        | Some (WireToken (wireName)) -> Some (WireInput (Wire (wireName, Signal 0)))
        | _ -> None

    let inputs = inputTokens |> Seq.map parseI
    let output =
        match outputToken with
        | WireToken wireName -> Some (Wire (wireName, Signal 0))
        | _ -> None
    (inputs, operation, output)

let processCommands (wireStates : seq<Wire>) (inputs : seq<Inputs>) (operator : Operation) (output : Wire) =
    let signalValue input =
        match input with
            | SignalInput (Signal (v)) -> v
            | WireInput (Wire (_, Signal v)) -> v
    let sig1 = inputs |> Seq.head |> signalValue
    let outputSignal = 
        match operator with
        | AND ->
            let sig2 = inputs |> Seq.skip 1 |> Seq.head |> signalValue
            sig1 &&& sig2
        | _ -> 0
    let on = ""
    inputs |>
        Seq.map (fun i ->
            match i with
            | WireInput (Wire (wireName, sigVal)) when wireName = on ->
                WireInput (Wire (wireName, Signal outputSignal))
            | noMatch -> noMatch)