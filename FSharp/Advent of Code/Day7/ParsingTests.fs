﻿namespace Day7.Tests

open NUnit.Framework
open Swensen.Unquote
open Day7.Implementation

[<TestFixture>]
type public Parsing () =

    [<Test>]
    member public this.ParseAndOperation () =
        let (inputs, operation, output) = parseString "x AND y -> z"
        let expectedInputs = [| WireToken "x"; WireToken "y" |]
        Assert.That(inputs, Is.EquivalentTo(expectedInputs))
        operation =! Some(OperatorToken(AND))
        output =! WireToken "z"

    [<Test>]
    member public this.ParseLShiftOperation () =
        let (inputs, operation, output) = parseString "p LSHIFT 2 -> q"
        let expectedInputs = [| WireToken "p"; SignalToken 2 |]
        Assert.That(inputs, Is.EquivalentTo(expectedInputs))
        operation =! Some(OperatorToken(LSHIFT))
        output =! WireToken "q"

    [<Test>]
    member public this.ParseAssignment () =
        let (inputs, operation, output) = parseString "123 -> x"
        let expectedInputs = [| SignalToken 123 |]
        Assert.That(inputs, Is.EquivalentTo(expectedInputs))
        operation =! None
        output =! WireToken "x"

    [<Test>]
    member public this.ParseNotOperation () =
        let (inputs, operation, output) = parseString "NOT e -> f"
        let expectedInputs = [| WireToken "e"; |]
        Assert.That(inputs, Is.EquivalentTo(expectedInputs))
        operation =! Some(OperatorToken(NOT))
        output =! WireToken "f"
