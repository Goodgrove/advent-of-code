﻿namespace Day5.Part1.Tests

open Day5.Part1.Implementation
open NUnit.Framework
open Swensen.Unquote

[<TestFixture>]
type public SpecTests () =

    [<TestCase("ugknbfddgicrmopn")>]
    [<TestCase("aaa")>]
    member public this.NiceTests (testString) =
        let result = isNiceString testString
        result =! true

    [<TestCase("jchzalrnumimnmhp")>]
    [<TestCase("haegwjzuvuyypxyu")>]
    [<TestCase("dvszwmarrgswjxmb")>]
    member public this.NaughtTests (testString) =
        let result = isNiceString testString
        result =! false
