﻿namespace Day5.Part2.Tests

open Day5.Part2.Implementation
open NUnit.Framework
open Swensen.Unquote

[<TestFixture>]
type public SpecTests () =

    [<TestCase("qjhvhtzxzqqjkmpb ")>]
    [<TestCase("xxyxx ")>]
    member public this.NiceTests (testString) =
        let result = isNiceString2 testString
        result =! true

    [<TestCase("uurcxstgmygtbstg ")>]
    [<TestCase("ieodomkazucvgmuy ")>]
    member public this.NaughtTests (testString) =
        let result = isNiceString2 testString
        result =! false
