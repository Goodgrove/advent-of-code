﻿module Day5.Part2.Implementation

(* 
--- Part Two ---

Realizing the error of his ways, Santa has switched to a better model of
determining whether a string is naughty or nice. None of the old rules apply,
as they are all clearly ridiculous.

Now, a nice string is one with all of the following properties:

    * It contains a pair of any two letters that appears at least twice in the
string without overlapping, like xyxy (xy) or aabcdefgaa (aa), but not like aaa
(aa, but it overlaps).
    * It contains at least one letter which repeats with exactly one letter
 between them, like xyx, abcdefeghi (efe), or even aaa.

For example:

    * qjhvhtzxzqqjkmpb is nice because is has a pair that appears twice (qj) and
a letter that repeats with exactly one letter between them (zxz).
    * xxyxx is nice because it has a pair that appears twice and a letter that
repeats with one between, even though the letters used by each rule overlap.
    * uurcxstgmygtbstg is naughty because it has a pair (tg) but no repeat with a
single letter between them.
    * ieodomkazucvgmuy is naughty because it has a repeating letter with one
between (odo), but no pair that appears twice.

How many strings are nice under these new rules?

*)

let substrings (str : string) (n : int) =
    let iterLen = (str.Length - n)
    { 0 .. iterLen} |>
        Seq.map (fun nn -> str.Substring(nn, n))

let hasRepeatingPairs str =
    let pairs = substrings str 2
    let pairs2 = pairs |> Seq.mapi (fun idx pair -> (pair, idx))
    pairs2 |>
        Seq.exists (fun (pair, idx) ->
            pairs2 |>
                Seq.exists (fun (p2, ix2) -> p2 = pair && ((ix2 - idx) > 1)))

let hasSandwichedChars (str : string) =
    let triples = substrings str 3
    triples |> Seq.exists (fun s -> s.[0] = s.[2])

let isNiceString2 str =
    (hasRepeatingPairs str) && (hasSandwichedChars str)
