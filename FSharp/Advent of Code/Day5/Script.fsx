﻿// Learn more about F# at http://fsharp.net. See the 'F# Tutorial' project
// for more guidance on F# programming.
#load "Part1Implementation.fs"
#load "Part2Implementation.fs"

open System.IO
open Day5.Part1.Implementation
open Day5.Part2.Implementation

let input = File.ReadAllLines ("PuzzleInput.txt") |> Seq.filter (fun str -> str <> "")
let niceStrings1 = input |> Seq.filter isNiceString
let count = Seq.length niceStrings1

printfn "%d" count

let niceStrings2 = input |> Seq.filter isNiceString2
let count2 = Seq.length niceStrings2

printfn "%d" count2