﻿namespace Day_1_Tests.Part2

open Day_1
open FsUnit
open NUnit.Framework

[<TestFixture>]
type SpecificationTests() = 
    let mutable floorCalc = new FloorCalculator()

    [<SetUp>]
    member public this.Setup() =
        floorCalc <- new FloorCalculator()

    [<Test>]
    member public this.SpecTest1() =
        let result = floorCalc.FirstHitBasement(0, ")")
        result |> should equal 1

    [<Test>]
    member public this.SpecTest2() =
        let result = floorCalc.FirstHitBasement(0, "()())")
        result |> should equal 5
