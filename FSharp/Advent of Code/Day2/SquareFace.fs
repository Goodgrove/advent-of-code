﻿namespace Day2.PaperCalculator

type public SquareFace(w, h) =
    member this.Width = w
    member this.Height = h

    static member Parse(str:string) =
        let parts = str.Split('x') |> Array.map int
        new SquareFace(parts.[0], parts.[1])
