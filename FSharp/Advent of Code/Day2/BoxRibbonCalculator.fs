﻿namespace Day2.PaperCalculator

type BoxRibbonCalculator() = 
    static member TotalRibbonRequired (box:Box) =
        box.SmallestFacePerimeter + box.Volume