﻿namespace Day2.PaperCalculator

type public Box(l, w, h) =
    member this.Length = l
    member this.Width = w
    member this.Height = h

    static member Parse(str:string) =
        let parts = str.Split('x') |> Array.map int
        new Box(parts.[0], parts.[1], parts.[2])

    member public this.SmallestFace  =
        if (this.Length <= this.Height && this.Width <= this.Height)
        then new SquareFace(this.Width, this.Length)
        elif (this.Length <= this.Width && this.Height <= this.Width)
        then new SquareFace(this.Length, this.Height)
        else new SquareFace(this.Width, this.Height)

    member public this.SmallestFaceArea =
        let smallest = this.SmallestFace
        smallest.Width * smallest.Height

    member public this.SmallestFacePerimeter =
        let smallest = this.SmallestFace
        2 * (smallest.Width + smallest.Height)

            member public this.SurfaceArea =
        2 * this.Length * this.Width + 2 * this.Width * this.Height + 2 * this.Height * this.Length

    member public this.Volume =
        this.Width * this.Height * this.Length