﻿namespace Day2.PaperCalculator

type BoxPaperCalculator() = 
    static member TotalPaperRequired (box:Box) =
        box.SurfaceArea + box.SmallestFaceArea
